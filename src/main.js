import Vue from 'vue';
import App from './App.vue';
import Router from './services/Router';
import Store from './services/Store';
import ModuleBootstrap from './modules/ModuleBootstrap';

Vue.config.productionTip = false;

ModuleBootstrap.initializeModules();

new Vue({
  router: Router,
  store: Store,
  render: h => h(App)
}).$mount('#app');
