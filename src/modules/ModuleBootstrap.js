import Router from '../services/Router';
import Store from '../services/Store';
import SystemModule from './system/Module';
import BrochureModule from './brochure/Module';
import AppModule from './app/Module';

function initializeModules() {
  initModule(SystemModule);
  initModule(BrochureModule);
  initModule(AppModule);
}

function initModule(module) {
  Router.addRoutes(module.routes);
  module.storeModules.forEach(element => {
    console.info('registering vuex module: ' + module.name + element.namespace);
    Store.registerModule(module.name + element.namespace, element);
    Store.dispatch(module.name + element.namespace + '/init', null, { root: true });
  });
}

export default {
  initializeModules
};
