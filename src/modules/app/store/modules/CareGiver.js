import ErrorHandler from '../../../system/lib/ApiErrorHandler';
// import CareGiver from '../../api/CareGiver';

const state = {
  careGivers: []
};

const getters = {
  allCareGivers: state => state.careGivers
};

const actions = {
  init() {
    console.info('CareGiver initializing...');
    console.info('CareGiver Initialized');
  },
  async fetchCareGivers({ commit }) {
    try {
      // override actual api call for sharing purposes
      // const careGivers = await CareGiver.getCareGivers();
      const careGivers = [
        { first_name: 'John', last_name: 'Doe' },
        { first_name: 'Mary', last_name: 'Poppins' }
      ];
      commit('setCareGivers', careGivers);
    } catch (error) {
      ErrorHandler.handleError(error);
    }
  }
};

const mutations = {
  setCareGivers: (state, careGivers) => {
    state.careGivers = careGivers;
  }
};

export default {
  namespaced: true,
  namespace: '/caregiver',
  state,
  getters,
  actions,
  mutations
};
