import ErrorHandler from '../../../system/lib/ApiErrorHandler';
import Patient from '../../api/Patient';

const state = {
  patients: []
};

const getters = {
  allPatients: state => state.patients
};

const actions = {
  init() {
    console.info('Patient initializing...');
    console.info('Patient Initialized');
  },
  async fetchPatients({ commit }) {
    try {
      const patients = await Patient.getPatients();
      commit('setPatients', patients.data);
    } catch (error) {
      ErrorHandler.handleError(error);
    }
  }
};

const mutations = {
  setPatients: (state, patients) => {
    state.patients = patients;
  }
};

export default {
  namespaced: true,
  namespace: '/patient',
  state,
  getters,
  actions,
  mutations
};
