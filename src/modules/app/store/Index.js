import Patient from './modules/Patient';
import CareGiver from './modules/CareGiver';

export default [
  Patient,
  CareGiver
];
