import AppLayout from '../components/layout/AppLayout.vue';
import AppHome from '../pages/Home.vue';
import Notifications from '../pages/Notifications.vue';
import CareGivers from '../pages/CareGivers.vue';

export default [
  {
    path: '/app',
    component: AppLayout,
    children: [
      { path: '', component: AppHome },
      { path: 'notifications', component: Notifications },
      { path: 'caregivers', component: CareGivers }
    ]
  }
];
