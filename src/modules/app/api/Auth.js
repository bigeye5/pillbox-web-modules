import axios from 'axios';

const client = axios.create({
  baseURL: process.env.VUE_APP_PILLBOX_SERVICE_URL
});

export default {
  logon(credentials) {
    return client.post('/logon', credentials);
  }
};
