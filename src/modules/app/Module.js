import Routes from './routes/Index';
import StoreModules from './store/Index';

export default {
  name: 'app',
  routes: Routes,
  storeModules: StoreModules
};
