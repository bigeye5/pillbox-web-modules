import BrochureLayout from '../components/layout/BrochureLayout.vue';
import BrochureHome from '../pages/Home.vue';
import Register from '../pages/Register.vue';
import Features from '../pages/Features.vue';

export default [
  {
    path: '/',
    component: BrochureLayout,
    children: [
      { path: '', component: BrochureHome },
      { path: 'register', component: Register },
      { path: 'features', component: Features }
    ]
  }
];
