const state = {};

const getters = {};

const actions = {
  init() {
    console.info('Blog initializing...');
    console.info('Blog Initialized');
  }
};

const mutations = {};

export default {
  namespaced: true,
  namespace: '/blog',
  state,
  getters,
  actions,
  mutations
};
