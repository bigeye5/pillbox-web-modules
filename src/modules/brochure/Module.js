import Routes from './routes/Index';
import StoreModules from './store/Index';

export default {
  name: 'brochure',
  routes: Routes,
  storeModules: StoreModules
};
