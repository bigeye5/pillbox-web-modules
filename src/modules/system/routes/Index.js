import NotFound from '../pages/NotFound.vue';

export default [{ path: '*', component: NotFound }];
