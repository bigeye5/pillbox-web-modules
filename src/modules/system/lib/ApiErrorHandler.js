import { EventBusErrorHandler } from './EventBusErrorHandler';
import SystemConstants from './SystemConstants';

export default {
  handleError(error) {
    if (error.response) {
      // server returns error
      if (error.response.status == SystemConstants.HTTP_UNAUTHORIZED) {
        EventBusErrorHandler.handleUnAuthorized();
      }
    } else if (error.request) {
      // request made but no response
      return {
        status: SystemConstants.HTTP_SERVICE_UNAVAILABLE,
        data: 'Service unavailable',
        message: 'Service unavailable'
      };
    } else {
      // something went wrong with the request
      return {
        status: SystemConstants.HTTP_INTERNAL_SERVER_ERROR,
        data: 'Internal Server Error',
        message: 'Internal Server Error'
      };
    }
  }
};
