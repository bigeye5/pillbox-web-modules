import Vue from 'vue';
import Router from '../../../services/Router';

export const EventBusErrorHandler = new Vue({
  methods: {
    handleUnAuthorized() {
      Router.push('/');
    }
  }
});
