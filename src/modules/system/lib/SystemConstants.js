export default {
  SUCCESS: { msg: 'Success' },
  GENERAL_ERROR: { msg: 'Error, could not process request' },
  LOGON_ERROR: { msg: 'Logon Error' },
  NOT_FOUND: { msg: 'Not Found' },
  HTTP_OK: 200,
  HTTP_CREATED: 201,
  HTTP_BAD_REQUEST: 400,
  HTTP_UNAUTHORIZED: 401,
  HTTP_NOT_FOUND: 404,
  HTTP_INTERNAL_SERVER_ERROR: 500,
  HTTP_SERVICE_UNAVAILABLE: 503
};
