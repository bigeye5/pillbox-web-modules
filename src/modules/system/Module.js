import Routes from './routes/Index';
import StoreModules from './store/Index';

export default {
  name: 'system',
  routes: Routes,
  storeModules: StoreModules
};
